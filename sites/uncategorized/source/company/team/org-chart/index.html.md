---
layout: markdown_page
title: Org Chart
manual_cookiebot: true
---

The org chart is available to all our team members through [this url](https://comp-calculator.gitlab.net/org_chart).
